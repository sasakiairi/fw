package jp.co.kenshu.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.CommentDto;
import jp.co.kenshu.dto.MessageDto;
import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.form.MessageForm;
import jp.co.kenshu.service.MessageService;

@Controller
public class HomeController {

	@Autowired
	private MessageService messageService;

	@RequestMapping(value = "/home/{id}", method = RequestMethod.GET)
	public String message(Model model, @PathVariable int id) {
		MessageDto message = messageService.getMessage(id);
		model.addAttribute("message", "20ch投稿表示");
		model.addAttribute("message", message);
		return "home";
	}

	@RequestMapping(value = "/home/", method = RequestMethod.GET)
	public String MessageAll(Model model) {
		List<MessageDto> messages = messageService.getMessageAll();
		List<CommentDto> comments = messageService.getCommentAll();
		CommentForm form = new CommentForm();
		model.addAttribute("commentForm", form);
		model.addAttribute("messages" , messages);
		model.addAttribute("comments" , comments);
		return "messageAll";
	}

	@RequestMapping(value = "/message/", method = RequestMethod.GET)
	public String messageInsert(Model model) {
		MessageForm form = new MessageForm();
		model.addAttribute("messageForm", form);
		return "messageInsert";
	}

	@RequestMapping(value = "/message/", method = RequestMethod.POST)
	public String messageInsert(@ModelAttribute MessageForm form, Model model) {
		int count = messageService.insertMessage(form.getMessage());
		Logger.getLogger(messageInsert(model)).log(Level.INFO, "新規投稿件数は" + count + "件です。");
		return "redirect:/home/";
	}

	@RequestMapping(value = "/home/", method = RequestMethod.POST)
	public String commentInsert(@ModelAttribute CommentForm form, Model model) {
		int count = messageService.insertComment(form.getComment(), form.getMessageId());
		Logger.getLogger(commentInsert(form,model)).log(Level.INFO, "コメント件数は" + count + "件です。");
		return "redirect:/home/";
	}

}
