package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.CommentDto;
import jp.co.kenshu.dto.MessageDto;
import jp.co.kenshu.entity.Comment;
import jp.co.kenshu.entity.Message;
import jp.co.kenshu.mapper.Mapper;

@Service
public class MessageService {

	@Autowired
	private Mapper messageMapper;

	public MessageDto getMessage(Integer id) {
		MessageDto dto = new MessageDto();
		Message entity = messageMapper.getMessage(id);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	public List<MessageDto> getMessageAll() {
		List<Message> messageList = messageMapper.getMessageAll();
		List<MessageDto> resultList = convertToDto(messageList);
		return resultList;
	}

	private List<MessageDto> convertToDto(List<Message> messageList) {
		List<MessageDto> resultList = new LinkedList<>();
		for (Message entity : messageList) {
			MessageDto dto = new MessageDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public int insertMessage(String message) {
		int count = messageMapper.insertMessage(message);
		return count;
	}

	public CommentDto getComment(Integer id) {
		CommentDto dto = new CommentDto();
		Comment entity = messageMapper.getComment(id);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	public List<CommentDto> getCommentAll() {
		List<Comment> commentList = messageMapper.getCommentAll();
		List<CommentDto> resultList = convertToDtoC(commentList);
		return resultList;
	}

	private List<CommentDto> convertToDtoC(List<Comment> commentList) {
		List<CommentDto> resultList = new LinkedList<>();
		for (Comment entity : commentList) {
			CommentDto dto = new CommentDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public int insertComment(String comment, int messageId) {
		int count = messageMapper.insertComment(comment, messageId);
		return count;
	}

}
