package jp.co.kenshu.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.Comment;
import jp.co.kenshu.entity.Message;

public interface Mapper {
	Message getMessage(int id);
	List<Message> getMessageAll();
	List<Comment> getCommentAll();
	int insertMessage(String message);
	Date createdDate();
	Comment getComment(int id);
	int insertComment(@Param("comment") String comment, @Param("message_id") int messageId);


}
