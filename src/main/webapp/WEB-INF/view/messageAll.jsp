<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<title>トップ画面</title>
</head>
<body>

<div class="header">
	<a href="/20ch/message/">新規投稿</a>
</div>
	<h1>20ch投稿一覧</h1>

	<c:forEach items="${messages}" var="message">
		<p>
			<c:out value="${message.message}"></c:out>
		</p>

		<c:forEach items="${comments}" var="comment">
			<c:if test="${comment.messageId == message.id }">
				<c:out value="${comment.comment}"></c:out>
			</c:if>
		</c:forEach>

		<form:form modelAttribute="commentForm">
			<form:input path="comment" />
			<input type="hidden" name="messageId" value="${message.id}">
			<input type="submit">
		</form:form>

	</c:forEach>




</body>
</html>

